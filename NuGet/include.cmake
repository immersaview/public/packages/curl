cmake_minimum_required(VERSION 3.0)

# Declaring variables
set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")
set(LIB_DEBUG_SUFFIX "")

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "aarch64")
    set(ARCH_DIR "arm64")
elseif ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

if (UNIX)
    find_program(LSB_RELEASE_EXEC lsb_release)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} --codename
        OUTPUT_VARIABLE LSB_RELEASE_CODENAME
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (LSB_RELEASE_CODENAME MATCHES "xenial")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu16_04/gcc5_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "bionic")    
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu18_04/gcc7_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "focal")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu20_04/gcc9_3_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "Core")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/centos7/gcc7_2_1/${ARCH_DIR}")
    endif ()

    set(LIB_DEBUG_SUFFIX "-d")
elseif (WIN32)
    set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/windows/vc142/${ARCH_DIR}")
    set(LIB_DEBUG_SUFFIX "d")
endif ()

function(add_imported_lib PLATFORM_DIR LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" STATIC IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${PLATFORM_DIR}/include")
    if (${CMAKE_VERSION} VERSION_GREATER 3.11.0)
        target_compile_definitions("${LIB}" INTERFACE CURL_STATICLIB)
    endif ()

    if (UNIX)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${PLATFORM_DIR}/debug/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${LIB_DEBUG_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    elseif (WIN32)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/lib${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${PLATFORM_DIR}/debug/lib${LIB}${LIB_DEBUG_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    endif ()
endfunction(add_imported_lib PLATFORM_DIR LIB)

add_imported_lib("${PLATFORM_DIR}" "curl")
