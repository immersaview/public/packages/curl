# Nuget packaging for Curl Repository #

## Example CMake Usage:
```cmake
target_link_libraries(target curl)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:curl,INTERFACE_INCLUDE_DIRECTORIES>")

# Curl Dependencies
add_definitions(-DCURL_STATICLIB)
target_link_libraries(target zlib ssl crypto)
if (WIN32)
    target_link_libraries(target wldap32)
endif ()

# OpenSSL Dependencies
find_package(Threads)
if (UNIX)
    target_link_libraries(target dl ${CMAKE_THREAD_LIBS_INIT})
elseif (WIN32)
    target_link_libraries(target crypt32 ws2_32 ${CMAKE_THREAD_LIBS_INIT})
endif ()
```